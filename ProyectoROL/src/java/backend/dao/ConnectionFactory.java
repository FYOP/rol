/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author roidz
 */
public class ConnectionFactory {
    
    private static ConnectionFactory instancia;
    String connectionUrl = "jdbc:sqlserver://localhost\\SQLEXPRESS:1433;databaseName=TPAPI;user=tp;password=tp";
    
    
    private ConnectionFactory() throws ClassNotFoundException{
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
    }
    
    public static ConnectionFactory getInstancia() throws ClassNotFoundException{
        if(instancia== null)
            instancia = new ConnectionFactory();
        
        return instancia;
    }
    
    public Connection getConection() throws SQLException{
        return DriverManager.getConnection(connectionUrl);
        
    }
}
